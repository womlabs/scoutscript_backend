package com.womlabs.scoutscript.service;

import com.womlabs.scoutscript.dto.TeamDto;
import com.womlabs.scoutscript.entity.Team;
import com.womlabs.scoutscript.repository.TeamRepository;
import com.womlabs.scoutscript.repository.UserRepository;
import com.womlabs.scoutscript.repository.UserToTeamRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class TeamService {

    private final TeamRepository teamRepository;
    private final UserToTeamRepository userToTeamRepository;
    private final UserRepository userRepository;

    public TeamDto getTeam(String teamUuid) {
        return new TeamDto(teamRepository.getByExternalUuid(teamUuid));
    }

    public TeamDto createTeam(TeamDto newTeamDto) {

        var team = Team.builder()
                .name(newTeamDto.getName())
                .parent(teamRepository.getByExternalUuid(newTeamDto.getParentExternalUuid()))
                .externalUuid(UUID.randomUUID().toString())
                .build();

        boolean saved = false;
        while (!saved) {
            try {
                team = teamRepository.save(team);
                saved = true;
            } catch (Exception e) {
                team.setExternalUuid(UUID.randomUUID().toString());
            }
        }
        newTeamDto.setExternalUuid(team.getExternalUuid());
        return newTeamDto;
    }


    public List<TeamDto> getAllTeamsByUsername(String username) {
        var user = userRepository.getByUsername(username);
        var userToTeamList = userToTeamRepository.getAllByUserId(user.getId());
        var returnList = new ArrayList<TeamDto>();
        for (var userToTeam: userToTeamList) {
            var team = teamRepository.getById(userToTeam.getTeamId());
            returnList.add(new TeamDto(team));
        }
        return returnList;
    }
}
