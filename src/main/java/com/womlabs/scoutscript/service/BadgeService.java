package com.womlabs.scoutscript.service;

import com.womlabs.scoutscript.dto.BadgeDto;
import com.womlabs.scoutscript.entity.Badge;
import com.womlabs.scoutscript.entity.BadgeElement;
import com.womlabs.scoutscript.repository.BadgeElementRepository;
import com.womlabs.scoutscript.repository.BadgeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class BadgeService {
    private final BadgeRepository badgeRepository;
    private final BadgeElementRepository badgeElementRepository;

    public BadgeDto createNewBadge(BadgeDto badgeDto) {
        var newBadge = Badge.builder()
                .name(badgeDto.getName())
                .numberOfElements(badgeDto.getElements().size())
                .build();

        newBadge = badgeRepository.save(newBadge);
        badgeDto.setId(newBadge.getId());
        for (var badgeElementDto : badgeDto.getElements()) {
            var newBadgeElement = BadgeElement.builder()
                    .badge(newBadge)
                    .description(badgeElementDto.getDescription())
                    .position(badgeElementDto.getPosition())
                    .build();

            newBadgeElement = badgeElementRepository.save(newBadgeElement);
            badgeElementDto.setId(newBadgeElement.getId());
        }
        return badgeDto;
    }
}
