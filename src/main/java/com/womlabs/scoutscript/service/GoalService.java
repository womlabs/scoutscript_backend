package com.womlabs.scoutscript.service;

import com.womlabs.scoutscript.dto.GoalDto;
import com.womlabs.scoutscript.entity.Goal;
import com.womlabs.scoutscript.repository.GoalRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class GoalService {

    private final GoalRepository goalRepository;

    public GoalDto createNewGoal(GoalDto newGoalDto) {
        var newGoal = Goal.builder()
                .description(newGoalDto.getDescription())
                .name(newGoalDto.getName())
                .methodDescription(newGoalDto.getMethodsDescription())
                .build();

        newGoal = goalRepository.save(newGoal);
        newGoalDto.setId(newGoal.getId());
        return newGoalDto;
    }
}
