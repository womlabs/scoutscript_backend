package com.womlabs.scoutscript.service;

import com.womlabs.scoutscript.controller.DocumentationType;
import com.womlabs.scoutscript.dto.*;
import com.womlabs.scoutscript.entity.*;
import com.womlabs.scoutscript.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
@Transactional
public class DocumentationService {
    private final DocumentationRepository documentationRepository;
    private final DocumentationElementRepository documentationElementRepository;
    private final DocumentationElementToBadgeElementRepository documentationElementToBadgeElementRepository;
    private final DocumentationElementToGoalRepository documentationElementToGoalRepository;
    private final DocumentationSummaryInformationRepository documentationSummaryInformationRepository;
    private final DocumentationSummaryLinkRepository documentationSummaryLinkRepository;
    private final BadgeElementRepository badgeElementRepository;
    private final GoalRepository goalRepository;
    private final TeamRepository teamRepository;

    public DocumentationDto getByExternalUuid(String externalUuid, String teamUuid) {
        var team = teamRepository.getByExternalUuid(teamUuid);
        if (team == null) return null;

        Documentation documentation = documentationRepository.getByExternalUuid(externalUuid);
        if (documentation == null) return null;

        var returnDocumentationDtoBuilder = DocumentationDto.builder()
                .name(documentation.getName())
                .eventDate(documentation.getEventDate())
                .eventDescription(documentation.getEventDescription())
                .externalUuid(documentation.getExternalUuid());

        List<DocumentationElement> documentationElements =
                documentationElementRepository.getAllByDocumentation(documentation);
        var returnDocumentationElements = new ArrayList<DocumentationElementDto>();
        for (var documentationElement : documentationElements) {
            var returnDocumentationElementBuilder = DocumentationElementDto.builder()
                    .name(documentationElement.getName())
                    .description(documentationElement.getDescription())
                    .duration(documentationElement.getDuration())
                    .position(documentationElement.getPosition())
                    .toPrepare(documentationElement.getToPrepare())
                    .externalUuid(documentationElement.getExternalUuid());
            var badgeElementsLinks = documentationElementToBadgeElementRepository
                    .getAllDocumentationElementToBadgeElementByDocumentationElement(documentationElement);
            if (badgeElementsLinks != null && !badgeElementsLinks.isEmpty()) {
                var badgeElements = new ArrayList<BadgeElementDto>();
                for (var link : badgeElementsLinks) {
                    var badgeElement = link.getBadgeElement();
                    badgeElements.add(
                            BadgeElementDto.builder()
                                    .id(badgeElement.getId())
                                    .description(badgeElement.getDescription())
                                    .position(badgeElement.getPosition())
                                    .badgeId(badgeElement.getBadge().getId())
                                    .badgeName(badgeElement.getBadge().getName())
                                    .isSummary(link.getIsSummary())
                                    .realisation(link.getRealisation())
                                    .comments(link.getComments())
                                    .build()
                    );
                }
                returnDocumentationElementBuilder.badgeElements(badgeElements);
            }
            var goalsLinks = documentationElementToGoalRepository.getAllDocumentationElementToGoalByDocumentationElement(documentationElement);
            if (goalsLinks != null && !goalsLinks.isEmpty()) {
                var goals = new ArrayList<GoalDto>();
                for (var link : goalsLinks) {
                    var goal = link.getGoal();
                    goals.add(
                            GoalDto.builder()
                                    .id(goal.getId())
                                    .name(goal.getName())
                                    .description(goal.getDescription())
                                    .methodsDescription(goal.getMethodDescription())
                                    .isSummary(link.getIsSummary())
                                    .realisation(link.getRealisation())
                                    .comments(link.getComments())
                                    .build()
                    );
                }
                returnDocumentationElementBuilder.goals(goals);
            }
            returnDocumentationElements.add(returnDocumentationElementBuilder.build());
        }
        returnDocumentationDtoBuilder.documentationElements(returnDocumentationElements);

        var documentationSummaryLink = documentationSummaryLinkRepository.getByActualDocumentation(documentation);

        if (documentationSummaryLink != null) {
            var documentationSummaryInformation = documentationSummaryInformationRepository.getByDocumentation(documentation);

            returnDocumentationDtoBuilder.referenceDocumentationUuid(documentationSummaryLink.getPlannedDocumentation().getExternalUuid());
            returnDocumentationDtoBuilder.pluses(documentationSummaryInformation.getPluses());
            returnDocumentationDtoBuilder.minuses(documentationSummaryInformation.getMinuses());
            returnDocumentationDtoBuilder.comments(documentationSummaryInformation.getComments());
        }

        return returnDocumentationDtoBuilder.build();
    }

    public DocumentationDto createNewDocumentation(DocumentationDto documentationDto, String teamUuid, DocumentationType type) {

        // TODO: add non-existing team handling
        var team = teamRepository.getByExternalUuid(teamUuid);
        if (team == null) return null;

        var returnDocumentationDtoBuilder = DocumentationDto.builder();

        Documentation documentation = createDocumentationFromDto(documentationDto, teamUuid);
        documentation.setExternalUuid(UUID.randomUUID().toString());

        // ensure correct UUID creation
        boolean saved = false;
        while (!saved) {
            try {
                documentation = documentationRepository.save(documentation);
                saved = true;
            } catch (DuplicateKeyException e) {
                documentation.setExternalUuid(UUID.randomUUID().toString());
            }
        }

        returnDocumentationDtoBuilder
                .name(documentation.getName())
                .eventDate(documentation.getEventDate())
                .eventDescription(documentation.getEventDescription())
                .externalUuid(documentation.getExternalUuid());

        var returnDocumentationElementDtoList = new ArrayList<DocumentationElementDto>();
        for (DocumentationElementDto documentationElementDto : documentationDto.getDocumentationElements()) {
            DocumentationElement documentationElement = createDocumentationElementFromDto(documentation, documentationElementDto);

            // save boundings to correct table

            var returnDocumentationElementBuilder = DocumentationElementDto.builder()
                    .name(documentationElement.getName())
                    .description(documentationElement.getDescription())
                    .duration(documentationElement.getDuration())
                    .position(documentationElement.getPosition())
                    .toPrepare(documentationElement.getToPrepare())
                    .externalUuid(documentationElement.getExternalUuid())
                    .badgeElements(documentationElementDto.getBadgeElements())
                    .goals(documentationElementDto.getGoals());
            returnDocumentationElementDtoList.add(returnDocumentationElementBuilder.build());
        }
        returnDocumentationDtoBuilder.documentationElements(returnDocumentationElementDtoList);

        if (type == DocumentationType.summary) {
            var referenceDocumentation = documentationRepository.getByExternalUuid(documentationDto.getReferenceDocumentationUuid());
            var documentationSummaryLink = DocumentationSummaryLink.builder()
                    .plannedDocumentation(referenceDocumentation)
                    .actualDocumentation(documentation)
                    .build();
            documentationSummaryLinkRepository.save(documentationSummaryLink);
            returnDocumentationDtoBuilder.referenceDocumentationUuid(documentationDto.getReferenceDocumentationUuid());

            var documentationSummaryInformation = DocumentationSummaryInformation
                    .builder()
                    .documentation(documentation)
                    .pluses(documentationDto.getPluses())
                    .minuses(documentationDto.getMinuses())
                    .comments(documentationDto.getComments())
                    .build();
            documentationSummaryInformationRepository.save(documentationSummaryInformation);
            returnDocumentationDtoBuilder
                    .pluses(documentationDto.getPluses())
                    .minuses(documentationDto.getMinuses())
                    .comments(documentationDto.getComments());
        }
        return returnDocumentationDtoBuilder.build();
    }

    public DocumentationDto updateDocumentation(DocumentationDto documentationDto, String teamUuid, DocumentationType type) {
        if (teamRepository.getByExternalUuid(teamUuid) == null) return null;

        Documentation documentation = documentationRepository.getByExternalUuid(documentationDto.getExternalUuid());
        if (documentation == null) return null;

        var returnDocumentationDtoBuilder = DocumentationDto.builder();

        documentation.setLastModificationDate(LocalDateTime.now());
        documentation.setName(documentationDto.getName());
        documentation.setEventDate(documentationDto.getEventDate());
        documentation.setEventDescription(documentationDto.getEventDescription());

        documentationRepository.save(documentation);

        returnDocumentationDtoBuilder
                .name(documentation.getName())
                .eventDate(documentation.getEventDate())
                .eventDescription(documentation.getEventDescription())
                .externalUuid(documentation.getExternalUuid());

        var returnDocumentationElementDtoList = new ArrayList<DocumentationElementDto>();
        // create and update documentationElements
        for (var documentationElementDto : documentationDto.getDocumentationElements()) {
            var documentationElement = documentationElementRepository.getByExternalUuid(documentationElementDto.getExternalUuid());
            if (documentationElement == null) {
                documentationElement = createDocumentationElementFromDto(documentation, documentationElementDto);
                var returnDocumentationElementBuilder = DocumentationElementDto.builder()
                        .name(documentationElement.getName())
                        .description(documentationElement.getDescription())
                        .duration(documentationElement.getDuration())
                        .position(documentationElement.getPosition())
                        .toPrepare(documentationElement.getToPrepare())
                        .externalUuid(documentationElement.getExternalUuid())
                        .badgeElements(documentationElementDto.getBadgeElements())
                        .goals(documentationElementDto.getGoals());
                returnDocumentationElementDtoList.add(returnDocumentationElementBuilder.build());
            } else {
                documentationElement.setName(documentationElementDto.getName());
                documentationElement.setDescription(documentationElementDto.getDescription());
                documentationElement.setDuration(documentationElementDto.getDuration());
                documentationElement.setPosition(documentationElementDto.getPosition());
                documentationElement.setToPrepare(documentationElementDto.getToPrepare());

                documentationElement = documentationElementRepository.save(documentationElement);

                if (documentationElementDto.getBadgeElements() != null) {
                    for (var badgeElement : documentationElementDto.getBadgeElements()) {
                        var documentationElementToBadgeElement = documentationElementToBadgeElementRepository
                                .getDocumentationElementToBadgeElementByDocumentationElementAndBadgeElement(
                                        documentationElement, badgeElementRepository.getById(badgeElement.getId()));

                        documentationElementToBadgeElement.setIsSummary(badgeElement.getIsSummary());
                        documentationElementToBadgeElement.setRealisation(badgeElement.getRealisation());
                        documentationElementToBadgeElement.setComments(badgeElement.getComments());
                        documentationElementToBadgeElementRepository.save(documentationElementToBadgeElement);
                    }
                }
                if (documentationElementDto.getGoals() != null) {
                    for (var goal : documentationElementDto.getGoals()) {
                        var documentationElementToGoal = documentationElementToGoalRepository
                                .getDocumentationElementToGoalByDocumentationElementAndGoal(documentationElement, goalRepository.getById(goal.getId()));
                        documentationElementToGoal.setIsSummary(goal.getIsSummary());
                        documentationElementToGoal.setRealisation(goal.getRealisation());
                        documentationElementToGoal.setComments(goal.getComments());
                        documentationElementToGoalRepository.save(documentationElementToGoal);
                    }
                }

                var returnDocumentationElementBuilder = DocumentationElementDto.builder()
                        .name(documentationElement.getName())
                        .description(documentationElement.getDescription())
                        .duration(documentationElement.getDuration())
                        .position(documentationElement.getPosition())
                        .toPrepare(documentationElement.getToPrepare())
                        .externalUuid(documentationElement.getExternalUuid())
                        .badgeElements(documentationElementDto.getBadgeElements())
                        .goals(documentationElementDto.getGoals());
                returnDocumentationElementDtoList.add(returnDocumentationElementBuilder.build());
            }
        }
        returnDocumentationDtoBuilder.documentationElements(returnDocumentationElementDtoList);
        // delete unhooked documentationElements
        var existingDocumentationElements = documentationElementRepository.getAllByDocumentation(documentation);
        for (var existingDocumentationElement : existingDocumentationElements) {
            boolean isNotInReturn = returnDocumentationElementDtoList.stream().filter(doc ->
                    Objects.equals(doc.getExternalUuid(), existingDocumentationElement.getExternalUuid())).findAny().isEmpty();
            if (isNotInReturn) {
                documentationElementToBadgeElementRepository.deleteAllByDocumentationElement(existingDocumentationElement);
                documentationElementToGoalRepository.deleteAllByDocumentationElement(existingDocumentationElement);
                documentationElementRepository.deleteById(existingDocumentationElement.getId());
            }
        }

        if (type == DocumentationType.summary) {

            var documentationSummaryInformation = documentationSummaryInformationRepository.getByDocumentation(documentation);
            documentationSummaryInformation.setPluses(documentationDto.getPluses());
            documentationSummaryInformation.setMinuses(documentationDto.getMinuses());
            documentationSummaryInformation.setComments(documentationDto.getComments());
            documentationSummaryInformationRepository.save(documentationSummaryInformation);

            returnDocumentationDtoBuilder
                    .referenceDocumentationUuid(documentationDto.getReferenceDocumentationUuid())
                    .pluses(documentationDto.getPluses())
                    .minuses(documentationDto.getMinuses())
                    .comments(documentationDto.getComments());
        }

        return returnDocumentationDtoBuilder.build();
    }

    public boolean deleteDocumentation(String externalUuid, String teamUuid) {
        var team = teamRepository.getByExternalUuid(teamUuid);
        if (team == null) return false;

        Documentation documentation = documentationRepository.getByExternalUuid(externalUuid);
        if (documentation == null) return false;

        var documentationSummaryLink = documentationSummaryLinkRepository.getByActualDocumentation(documentation);
        if (documentationSummaryLink != null) {
            documentationSummaryLinkRepository.delete(documentationSummaryLink);
            documentationSummaryInformationRepository.deleteByDocumentation(documentation);
        }

        var documentationElements = documentationElementRepository.getAllByDocumentation(documentation);

        for (var documentationElement : documentationElements) {
            documentationElementToBadgeElementRepository.deleteAllByDocumentationElement(documentationElement);
            documentationElementToGoalRepository.deleteAllByDocumentationElement(documentationElement);
            documentationElementRepository.delete(documentationElement);
        }

        documentationRepository.delete(documentation);

        return true;
    }

    private Documentation createDocumentationFromDto(DocumentationDto documentationDto, String teamUuid) {
        return Documentation.builder()
                .name(documentationDto.getName())
                .eventDate(documentationDto.getEventDate())
                .eventDescription(documentationDto.getEventDescription())
                .createDate(LocalDateTime.now())
                .lastModificationDate(LocalDateTime.now())
                .team(teamRepository.getByExternalUuid(teamUuid))
                .build();
    }

    private DocumentationElement createDocumentationElementFromDto(Documentation documentation, DocumentationElementDto documentationElementDto) {
        var documentationElement = DocumentationElement.builder()
                .documentation(documentation)
                .name(documentationElementDto.getName())
                .description(documentationElementDto.getDescription())
                .duration(documentationElementDto.getDuration())
                .position(documentationElementDto.getPosition())
                .toPrepare(documentationElementDto.getToPrepare())
                .externalUuid(UUID.randomUUID().toString())
                .build();

        // ensure correct UUID creation
        boolean saved = false;
        while (!saved) {
            try {
                documentationElement = documentationElementRepository.save(documentationElement);
                saved = true;
            } catch (DuplicateKeyException e) {
                documentationElement.setExternalUuid(UUID.randomUUID().toString());
            }
        }

        if (documentationElementDto.getBadgeElements() != null) {
            for (var badgeElement : documentationElementDto.getBadgeElements()) {
                var documentationElementToBadgeElement = DocumentationElementToBadgeElement.builder()
                        .documentationElement(documentationElement)
                        .badgeElement(badgeElementRepository.getById(badgeElement.getId()))
                        .isSummary(badgeElement.getIsSummary())
                        .realisation(badgeElement.getRealisation())
                        .comments(badgeElement.getComments())
                        .build();
                documentationElementToBadgeElementRepository.save(documentationElementToBadgeElement);
            }
        }
        if (documentationElementDto.getGoals() != null) {
            for (var goal : documentationElementDto.getGoals()) {
                var documentationElementToGoal = DocumentationElementToGoal.builder()
                        .documentationElement(documentationElement)
                        .goal(goalRepository.getById(goal.getId()))
                        .isSummary(goal.getIsSummary())
                        .realisation(goal.getRealisation())
                        .comments(goal.getComments())
                        .build();
                documentationElementToGoalRepository.save(documentationElementToGoal);
            }
        }

        return documentationElement;
    }

    public List<ToPrepareDto> getAllToPrepareForTeam(String teamUuid) {
        List<ToPrepareDto> returnList = new ArrayList<>();
        var documentations = documentationRepository.getAllByTeam(teamRepository.getByExternalUuid(teamUuid));
        for (var documentation : documentations) {
            var documentationSummaryLink = documentationSummaryLinkRepository.getByActualDocumentation(documentation);
            if(documentationSummaryLink==null){
                var documentationElements = documentationElementRepository.getAllByDocumentation(documentation);
                List<String> toPrepareList = new ArrayList<>();
                for (var documentationElement : documentationElements) {
                    if (documentationElement.getToPrepare() != null && !documentationElement.getToPrepare().isEmpty()) {
                        toPrepareList.add(documentationElement.getToPrepare());
                    }
                }
                if (!toPrepareList.isEmpty()) {
                    returnList.add(ToPrepareDto.builder()
                            .name(documentation.getName() + " " + documentation.getEventDate().toString().split("T")[0].replaceAll("-", "."))
                            .toPrepareElements(toPrepareList.toString().substring(1, toPrepareList.toString().length() - 1))
                            .build());
                }
            }
        }
        return returnList;
    }

    public List<DocumentationDto> getAllDrafts(String teamUuid) {
        var documentations = documentationRepository.getAllByTeam(teamRepository.getByExternalUuid(teamUuid));
        var summaries = findAllSummariesInDocumentationList(documentations);
        return documentations
                .stream()
                .filter((v) ->  !summaries.contains(v))
                .map((v) -> createDocumentationDto(v, DocumentationType.draft))
                .collect(Collectors.toList());
    }

    public List<DocumentationDto> getAllSummaries(String teamUuid) {
        var documentations = documentationRepository.getAllByTeam(teamRepository.getByExternalUuid(teamUuid));
        var summaries = findAllSummariesInDocumentationList(documentations);
        return documentations
                .stream()
                .filter(summaries::contains)
                .map((v) -> createDocumentationDto(v, DocumentationType.summary))
                .collect(Collectors.toList());
    }

    private List<Documentation> findAllSummariesInDocumentationList(List<Documentation> documentationList) {
        var returnList = new ArrayList<Documentation>();
        for (var documentation : documentationList) {
            if (documentationSummaryLinkRepository.getByActualDocumentation(documentation) != null) {
                returnList.add(documentation);
            }
        }
        return returnList;
    }

    private DocumentationDto createDocumentationDto(Documentation documentation, DocumentationType type) {

        var documentationDtoBuilder = DocumentationDto.builder();

        documentationDtoBuilder
                .name(documentation.getName())
                .eventDate(documentation.getEventDate())
                .eventDescription(documentation.getEventDescription())
                .externalUuid(documentation.getExternalUuid());

        var returnDocumentationElementDtoList = new ArrayList<DocumentationElementDto>();
        for (DocumentationElement documentationElement : documentationElementRepository.getAllByDocumentation(documentation)) {

            var elementToGoals = documentationElementToGoalRepository
                    .getAllDocumentationElementToGoalByDocumentationElement(documentationElement);
            var elementToBadges = documentationElementToBadgeElementRepository
                    .getAllDocumentationElementToBadgeElementByDocumentationElement(documentationElement);

            var badgesDto = new ArrayList<BadgeElementDto>();
            var goals = new ArrayList<GoalDto>();

            for (var badge : elementToBadges) {
                var newBadge = BadgeElementDto.builder();
                newBadge.id(badge.getBadgeElement().getId())
                        .badgeId(badge.getBadgeElement().getBadge().getId())
                        .badgeName(badge.getBadgeElement().getBadge().getName())
                        .description(badge.getBadgeElement().getDescription())
                        .position(badge.getBadgeElement().getPosition())
                        .isSummary(badge.getIsSummary())
                        .realisation(badge.getRealisation())
                        .comments(badge.getComments());
                badgesDto.add(newBadge.build());
            }

            for (var goal : elementToGoals) {
                var newGoal = GoalDto.builder();
                newGoal.id(goal.getGoal().getId())
                        .name(goal.getGoal().getName())
                        .description(goal.getGoal().getDescription())
                        .methodsDescription(goal.getGoal().getMethodDescription())
                        .isSummary(goal.getIsSummary())
                        .realisation(goal.getRealisation())
                        .comments(goal.getComments());
                goals.add(newGoal.build());
            }

            var returnDocumentationElementBuilder = DocumentationElementDto.builder()
                    .name(documentationElement.getName())
                    .description(documentationElement.getDescription())
                    .duration(documentationElement.getDuration())
                    .position(documentationElement.getPosition())
                    .toPrepare(documentationElement.getToPrepare())
                    .externalUuid(documentationElement.getExternalUuid())
                    .badgeElements(badgesDto)
                    .goals(goals);
            returnDocumentationElementDtoList.add(returnDocumentationElementBuilder.build());
        }
        documentationDtoBuilder.documentationElements(returnDocumentationElementDtoList);

        if (type == DocumentationType.summary) {

            var documentationSummaryInformation = documentationSummaryInformationRepository.getByDocumentation(documentation);
            var documentationSummaryLink = documentationSummaryLinkRepository.getByActualDocumentation(documentation);

            documentationDtoBuilder.referenceDocumentationUuid(documentationSummaryLink.getPlannedDocumentation().getExternalUuid());

            documentationDtoBuilder
                    .pluses(documentationSummaryInformation.getPluses())
                    .minuses(documentationSummaryInformation.getMinuses())
                    .comments(documentationSummaryInformation.getComments());
        }
        return documentationDtoBuilder.build();
    }

}
