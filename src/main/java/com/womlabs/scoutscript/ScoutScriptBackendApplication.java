package com.womlabs.scoutscript;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScoutScriptBackendApplication {
    // TODO: add roles to requests (admin panel and mobile user)
    // TODO: services and controllers
    // TODO: populate badge data
    // TODO: add login
    // TODO: add login to team bindings
    // TODO: add team crud for admin role
    // TODO: add badge crud for admin role
    // TODO: add Zalando problems and throwing
    public static void main(String[] args) {
        SpringApplication.run(ScoutScriptBackendApplication.class, args);
    }

}
