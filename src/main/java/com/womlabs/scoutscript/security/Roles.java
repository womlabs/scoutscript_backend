package com.womlabs.scoutscript.security;

public enum Roles {
    ROLE_USER, ROLE_ADMIN
}