package com.womlabs.scoutscript.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table
@Getter
@Setter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class DocumentationElement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String externalUuid;

    private String name;

    private Integer position;

    private String description;

    private Integer duration;

    private String toPrepare;

    @ManyToOne
    @JoinColumn(name = "documentation_id")
    private Documentation documentation;
}