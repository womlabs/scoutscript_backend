package com.womlabs.scoutscript.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table
@Getter
@Setter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class DocumentationSummaryLink {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "planned_documentation_id")
    private Documentation plannedDocumentation;

    @OneToOne
    @JoinColumn(name = "actual_documentation_id")
    private Documentation actualDocumentation;
}
