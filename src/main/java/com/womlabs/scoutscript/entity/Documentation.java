package com.womlabs.scoutscript.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table
@Getter
@Setter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class Documentation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String externalUuid;

    private String name;

    private LocalDateTime eventDate;

    private String eventDescription;

    private LocalDateTime createDate;

    private LocalDateTime lastModificationDate;

    @ManyToOne
    @JoinColumn(name = "team_id")
    private Team team;
}
