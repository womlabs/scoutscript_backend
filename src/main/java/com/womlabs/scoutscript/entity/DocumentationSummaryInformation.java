package com.womlabs.scoutscript.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table
@Getter
@Setter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class DocumentationSummaryInformation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String pluses;

    private String minuses;

    private String comments;

    @OneToOne
    @JoinColumn(name = "documentation_id")
    private Documentation documentation;
}
