package com.womlabs.scoutscript.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table
@Getter
@Setter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class DocumentationElementToGoal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "documentation_element_id")
    private DocumentationElement documentationElement;

    @ManyToOne
    @JoinColumn(name = "goal_id")
    private Goal goal;

    private Boolean isSummary;

    private Boolean realisation;

    private String comments;
}
