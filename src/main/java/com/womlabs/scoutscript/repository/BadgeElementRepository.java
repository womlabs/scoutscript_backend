package com.womlabs.scoutscript.repository;

import com.womlabs.scoutscript.entity.BadgeElement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BadgeElementRepository extends JpaRepository<BadgeElement, Long> {
}
