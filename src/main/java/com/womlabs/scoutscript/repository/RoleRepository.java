package com.womlabs.scoutscript.repository;

import com.womlabs.scoutscript.entity.Role;
import com.womlabs.scoutscript.security.Roles;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(Roles name);
}
