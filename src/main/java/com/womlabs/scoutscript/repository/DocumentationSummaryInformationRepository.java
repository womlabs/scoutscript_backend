package com.womlabs.scoutscript.repository;

import com.womlabs.scoutscript.entity.Documentation;
import com.womlabs.scoutscript.entity.DocumentationSummaryInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentationSummaryInformationRepository extends JpaRepository<DocumentationSummaryInformation, Long> {
    DocumentationSummaryInformation getByDocumentation(Documentation documentation);
    void deleteByDocumentation(Documentation documentation);
}
