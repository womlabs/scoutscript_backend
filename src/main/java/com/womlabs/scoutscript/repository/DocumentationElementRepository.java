package com.womlabs.scoutscript.repository;

import com.womlabs.scoutscript.entity.Documentation;
import com.womlabs.scoutscript.entity.DocumentationElement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface DocumentationElementRepository extends JpaRepository<DocumentationElement, Long> {
    DocumentationElement getByExternalUuid(String externalUuid);
    List<DocumentationElement> getAllByDocumentation(Documentation documentation);
}
