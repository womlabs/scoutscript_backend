package com.womlabs.scoutscript.repository;

import com.womlabs.scoutscript.entity.Documentation;
import com.womlabs.scoutscript.entity.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DocumentationRepository extends JpaRepository<Documentation, Long> {
    Documentation getByExternalUuid(String externalUuid);

    List<Documentation> getAllByTeam(Team team);
}
