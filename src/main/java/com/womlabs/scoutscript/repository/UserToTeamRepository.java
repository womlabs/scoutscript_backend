package com.womlabs.scoutscript.repository;

import com.womlabs.scoutscript.entity.UserToTeam;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserToTeamRepository extends JpaRepository<UserToTeam, Long> {

    List<UserToTeam> getAllByUserId(Long userId);
}
