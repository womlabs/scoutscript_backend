package com.womlabs.scoutscript.repository;

import com.womlabs.scoutscript.entity.Documentation;
import com.womlabs.scoutscript.entity.DocumentationSummaryLink;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DocumentationSummaryLinkRepository extends JpaRepository<DocumentationSummaryLink, Long> {
    DocumentationSummaryLink getByActualDocumentation(Documentation actualDocumentation);
    DocumentationSummaryLink getByPlannedDocumentation(Documentation planned);
}
