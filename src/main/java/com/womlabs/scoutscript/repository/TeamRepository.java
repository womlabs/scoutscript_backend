package com.womlabs.scoutscript.repository;

import com.womlabs.scoutscript.entity.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeamRepository extends JpaRepository<Team, Long> {
    Team getByExternalUuid(String externalUuid);
}
