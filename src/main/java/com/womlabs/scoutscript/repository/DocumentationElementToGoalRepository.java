package com.womlabs.scoutscript.repository;

import com.womlabs.scoutscript.entity.DocumentationElement;
import com.womlabs.scoutscript.entity.DocumentationElementToGoal;
import com.womlabs.scoutscript.entity.Goal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface DocumentationElementToGoalRepository extends JpaRepository<DocumentationElementToGoal, Long> {
    List<DocumentationElementToGoal> getAllDocumentationElementToGoalByDocumentationElement(DocumentationElement documentationElement);
    DocumentationElementToGoal getDocumentationElementToGoalByDocumentationElementAndGoal(DocumentationElement documentationElement, Goal goal);

    void deleteAllByDocumentationElement(DocumentationElement documentationElement);
}
