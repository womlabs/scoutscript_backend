package com.womlabs.scoutscript.repository;

import com.womlabs.scoutscript.entity.BadgeElement;
import com.womlabs.scoutscript.entity.DocumentationElement;
import com.womlabs.scoutscript.entity.DocumentationElementToBadgeElement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface DocumentationElementToBadgeElementRepository extends JpaRepository<DocumentationElementToBadgeElement, Long> {
    List<DocumentationElementToBadgeElement> getAllDocumentationElementToBadgeElementByDocumentationElement(DocumentationElement documentationElement);
    DocumentationElementToBadgeElement getDocumentationElementToBadgeElementByDocumentationElementAndBadgeElement(DocumentationElement documentationElement, BadgeElement badgeElement);

    void deleteAllByDocumentationElement(DocumentationElement documentationElement);
}
