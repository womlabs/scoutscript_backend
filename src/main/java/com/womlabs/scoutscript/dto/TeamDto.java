package com.womlabs.scoutscript.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.womlabs.scoutscript.entity.Team;
import lombok.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class TeamDto {

    public TeamDto(Team team) {
        this.name = team.getName();
        this.externalUuid = team.getExternalUuid();
        if (team.getParent() != null)
            this.parentExternalUuid = team.getParent().getExternalUuid();
    }

    private String externalUuid;

    private String name;

    private String parentExternalUuid;
}
