package com.womlabs.scoutscript.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.time.LocalDateTime;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class DocumentationDto {

    private String externalUuid;

    private String name;

    private LocalDateTime eventDate;

    private String eventDescription;

    private List<DocumentationElementDto> documentationElements;

    private String referenceDocumentationUuid;

    private String pluses;

    private String minuses;

    private String comments;
}
