package com.womlabs.scoutscript.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class GoalDto {
    private Long id;

    private String name;

    private String description;

    private String methodsDescription;

    private Boolean isSummary;

    private Boolean realisation;

    private String comments;
}
