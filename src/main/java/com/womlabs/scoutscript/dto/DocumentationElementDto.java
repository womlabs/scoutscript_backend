package com.womlabs.scoutscript.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class DocumentationElementDto {

    private String externalUuid;

    private String name;

    private String description;

    private Integer position;

    private Integer duration;

    private String toPrepare;

    private List<BadgeElementDto> badgeElements;

    private List<GoalDto> goals;
}
