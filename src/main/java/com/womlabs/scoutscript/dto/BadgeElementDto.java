package com.womlabs.scoutscript.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class BadgeElementDto {

    private Long id;

    private String description;

    private Integer position;

    private Long badgeId;

    private String badgeName;

    private Boolean isSummary;

    private Boolean realisation;

    private String comments;
}
