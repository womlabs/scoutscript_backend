package com.womlabs.scoutscript.controller;

import com.womlabs.scoutscript.dto.DocumentationDto;
import com.womlabs.scoutscript.dto.ToPrepareDto;
import com.womlabs.scoutscript.service.DocumentationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@RestController
@RequestMapping("/api/v1/documentation")
@RequiredArgsConstructor
public class DocumentationController {

    private final DocumentationService documentationService;

    @GetMapping("/{externalUuid}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
    DocumentationDto getByExternalUuid(@PathVariable String externalUuid, @RequestParam String teamUuid) {
        return documentationService.getByExternalUuid(externalUuid, teamUuid);
    }

    @PostMapping("/{type}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
    DocumentationDto createDocumentation(@PathVariable DocumentationType type, @RequestParam String teamUuid,
                                         @RequestBody DocumentationDto newDocumentation) {
        return documentationService.createNewDocumentation(newDocumentation, teamUuid, type);
    }

    @PutMapping("/{type}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
    DocumentationDto updateDocumentation(@PathVariable DocumentationType type, @RequestParam String teamUuid,
                                         @RequestBody DocumentationDto newDocumentation) {
        return documentationService.updateDocumentation(newDocumentation, teamUuid, type);
    }

    @DeleteMapping("/{externalUuid}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
    HttpStatus deleteDocumentation(@PathVariable String externalUuid, @RequestParam String teamUuid) {
        return documentationService.deleteDocumentation(externalUuid, teamUuid) ? HttpStatus.NO_CONTENT :
                HttpStatus.BAD_REQUEST;
    }

    @GetMapping("/list/toprepare")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
    List<ToPrepareDto> getAllToPrepareForTeam(@RequestParam String teamUuid) {
        return documentationService.getAllToPrepareForTeam(teamUuid);
    }

    @GetMapping("/list/draft")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
    List<DocumentationDto> getAllDrafts(@RequestParam String teamUuid) {
        return documentationService.getAllDrafts(teamUuid);
    }

    @GetMapping("/list/summary")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
    List<DocumentationDto> getAllSummaries(@RequestParam String teamUuid) {
        return documentationService.getAllSummaries(teamUuid);
    }
}
