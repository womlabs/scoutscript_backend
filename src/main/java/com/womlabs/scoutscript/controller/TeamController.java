package com.womlabs.scoutscript.controller;

import com.womlabs.scoutscript.dto.TeamDto;
import com.womlabs.scoutscript.security.Roles;
import com.womlabs.scoutscript.service.TeamService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@RestController
@RequestMapping("/api/v1/teams")
@RequiredArgsConstructor
public class TeamController {

    private final TeamService teamService;

    @GetMapping("/{teamUuid}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
    public TeamDto getTeam(@PathVariable String teamUuid) {
        return teamService.getTeam(teamUuid);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public TeamDto createTeam(@RequestBody TeamDto newTeamDto) {
        return teamService.createTeam(newTeamDto);
    }

    @GetMapping("/list/{username}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
    public List<TeamDto> getAllTeamsByUsername(@PathVariable String username) {return teamService.getAllTeamsByUsername(username);}
}
