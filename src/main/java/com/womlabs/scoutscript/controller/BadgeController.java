package com.womlabs.scoutscript.controller;

import com.womlabs.scoutscript.dto.BadgeDto;
import com.womlabs.scoutscript.security.Roles;
import com.womlabs.scoutscript.service.BadgeService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;

@RestController
@RequestMapping("/api/v1/badge")
@RequiredArgsConstructor
class BadgeController {

    private final BadgeService badgeService;

    @PostMapping
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public BadgeDto createBadge(@RequestBody BadgeDto badgeDto) {
        return badgeService.createNewBadge(badgeDto);
    }

}