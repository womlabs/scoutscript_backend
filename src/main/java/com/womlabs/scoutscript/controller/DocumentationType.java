package com.womlabs.scoutscript.controller;

public enum DocumentationType {
    draft, summary
}
