package com.womlabs.scoutscript.controller;

import com.womlabs.scoutscript.dto.GoalDto;
import com.womlabs.scoutscript.service.GoalService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;

@RestController
@RequestMapping("/api/v1/goal")
@RequiredArgsConstructor
public class GoalController {
    private final GoalService goalService;

    @PostMapping
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
    public GoalDto createNewGoal(@RequestBody GoalDto newGoalDto) {
        return goalService.createNewGoal(newGoalDto);
    }
}
